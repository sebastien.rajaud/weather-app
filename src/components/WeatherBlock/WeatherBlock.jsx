import moment from "moment";

const WeatherBlock = ({item}) => {
    const convertFahrenheitToCelsius = (fahrenheit) => {
        const celsius = (fahrenheit - 32) * 5 / 9;
        return celsius;
    }

    return (
        <div className="weatherBlock">
            <div className="weatherBlock__date">{moment.unix(item.dt).format('DD/MM/Y HH:mm')}</div>
            <div className="weatherBlock__icon">
                <img src={`http://openweathermap.org/img/wn/${item.weather[0].icon}@2x.png`} alt=""/>
            </div>
            <div className="weatherBlock__infos">
                Max : {`${item.main.temp_max} C°`} <br/>
                Min : {`${item.main.temp_min} C°`} <br/>
                Humidité : {`${item.main.humidity} %`}
            </div>
            <div className="weatherBlock__desc">Temps : {item.weather[0].description}</div>
            <div className="weatherBlock__desc">Nuageux à {item.clouds.all}%</div>
        </div>
    )
}

export default WeatherBlock;