import {useState} from 'react';
import moment from "moment";
import {BsPlusCircle, BsDashCircleFill, BsCalendar} from "react-icons/bs";
import WeatherBlock from "./WeatherBlock";

const WeatherRow = ({dayData}) => {
    const [active, setActive] = useState(false)
    return (
        <div className="card__row">
            <div className="card__row-title" onClick={() => setActive(state => !state)}>
                <span><BsCalendar className="card__icon-calendar"/><span className="card__row-date">{moment(dayData[0]).format('DD/MM/YY')}</span></span>
                <button className="card__button" >
                    { active ? <BsDashCircleFill /> : <BsPlusCircle/> }
                </button>
            </div>
            {active &&
            <div className="card__grid">
                {dayData[1].map(item => <WeatherBlock key={item.dt} item={item}/>)}
            </div>
            }

        </div>
    )
}

export default WeatherRow;