import {useState} from 'react';

const Header = ({setSearch, ...otherProps}) => {

    const [value, setValue] = useState('');

    const handleChange = (e) => {
        setValue(state => e.target.value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        setSearch(value)
        setValue('')
    }

    return (
        <header {...otherProps}>
            <form onSubmit={handleSubmit} >
                <input type="text" name="location" value={value} onChange={handleChange}/>
                <button type="submit" disabled={value.length < 3}>OK</button>
            </form>
        </header>
    )
}

export default Header;