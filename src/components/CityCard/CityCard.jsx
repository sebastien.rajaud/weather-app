import {useState, useEffect} from 'react';
import moment from "moment";
import WeatherRow from "../WeatherBlock/WeatherRow";
import axios from "axios";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import {BiUser} from 'react-icons/bi';
import {WiSunrise, WiSunset} from 'react-icons/wi';
import {GiTargeting} from 'react-icons/gi'

const CityCard = ({cityData}) => {
    // console.log(cityData)
    // console.log(cityData.list)
    const [roadRisk, setRoadRisk] = useState(null);
    const [weatherGrouped, setWeatherGrouped] = useState([]);

    const getUrlWithTiles = (lat, lng, zoom) => {
        const x = Math.floor(((lng + 180) / 360) * Math.pow(2, zoom));
        const y = Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom));
console.log(x, ' - ' , y)
        return `https://tile.openweathermap.org/map/clouds_new/${zoom}/${x}/${y}.png?appid=${process.env.REACT_APP_WEATHER_API_KEY}`;

    }

    const groupBy = (groupedArray, groupedByDay) => {
        return groupedArray.reduce((acc, obj) => {
            const key = moment(obj[groupedByDay]).format('Y-MM-DD');
            !acc[key] ? (acc[key] = []) : acc[key].push(obj);
            return acc;
        }, {});
    };


    useEffect(async () => {
        setWeatherGrouped(groupBy(cityData.list, 'dt_txt'));
    }, [])

    return (
        <div className="card">
            <div className="card__header">
                <h1 className="card__title">{cityData.city.name}<span>{cityData.city.country}</span></h1>
                <div className="card__population"><BiUser className="card__icons" /> <strong>Population : </strong>{cityData.city.population} hab.</div>
                <div className="card__sunrise"><WiSunrise className="card__icons" /> <strong>Heure du lever
                    : </strong>{moment.unix(cityData.city.sunrise).format('HH:mm')}</div>
                <div className="card__sunrise"><WiSunset className="card__icons" /> <strong>Heure du coucher
                    : </strong>{moment.unix(cityData.city.sunset).format('HH:mm')}</div>
                <div className="card__position"><GiTargeting className="card__icons" /> <strong>Position : </strong>
                    <span>Lat : {cityData.city.coord.lat} / Long : {cityData.city.coord.lon}</span>
                </div>
            </div>
            <div className="card__map">
                <MapContainer
                    center={[cityData.city.coord.lat, cityData.city.coord.lon]}
                    zoom={12}
                >
                    <TileLayer
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                </MapContainer>
            </div>


            {cityData.list ?
                Object.entries(weatherGrouped).map((day, index) => (
                    <WeatherRow dayData={day} key={index} />
                ))
                : null
            }
        </div>
    )
}

export default CityCard;