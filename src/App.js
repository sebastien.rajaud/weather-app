import './App.css';
import './scss/main.scss';
import {useState, useEffect} from 'react';
import axios from "axios";
import 'leaflet/dist/leaflet.css';


import Header from "./components/Header/Header";
import CityCard from "./components/CityCard/CityCard";
import {RiLoader4Line} from "react-icons/ri";

function App() {
    const [search, setSearch] = useState(null);
    const [cityData, setCityData] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    const fetchWeather = async (city) => {

        try {
            const {data} = await axios.get(`https://api.openweathermap.org/data/2.5/forecast?q=${search.replace(/\s/g, '-')}&appid=${process.env.REACT_APP_WEATHER_API_KEY}&lang=fr&units=metric`)
            data && setCityData(data);
        } catch (error) {
            handleError(error)
        }

    }

    const handleError = (error) => {
        setError('Il n\'y a pas de résultat pour votre recherche')
    }

    const handleSearch = (value) => {
        setError(null)
        setIsLoading(true);
        setSearch(value);
    }


    useEffect(async () => {
        if (search) {
            await fetchWeather(search)
            setIsLoading(false);
        }
    }, [search])

    return (
        <>
            <Header setSearch={handleSearch} className="header"/>
            <div className="wrapper content">
                {error && <div className="message alert">{error}</div>}
                {isLoading ? <RiLoader4Line className="loader"/> :
                    cityData && <CityCard cityData={cityData}/>
                }
            </div>
        </>
    );
}

export default App;
